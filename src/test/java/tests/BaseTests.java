package tests;

import navigationtiming.PerfNavigationTiming;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import pages.*;

import static util.Constants.*;

public class BaseTests {

    private static WebDriver driver;
    private static final String DEMO_URL = "https://demo.nopcommerce.com/";
    protected PerfNavigationTiming perfNavigationTiming = new PerfNavigationTiming();

    @BeforeTest
    public void profileSetUp() {
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
    }

    @BeforeMethod
    public void testsSetUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(DEMO_URL);
        HOME_PAGE = new HomePage(driver);
        DESKTOPS_PAGE = new DesktopsPage(driver);
        COMPUTERS_PAGE = new ComputersPage(driver);
        PRODUCT_PAGE = new ProductPage(driver);
    }

    @AfterMethod
    public void tearDown() {
        driver.close();
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public BasePage getBasePage() {
        return new BasePage(getDriver());
    }

    public HomePage getHomePage(){
        return new HomePage(getDriver());
    }

    public ComputersPage getComputersPage() {
        return new ComputersPage(getDriver());
    }

    public DesktopsPage getDesktopsPage() {
        return new DesktopsPage(getDriver());
    }

    public ProductPage getProductPage(){
        return new ProductPage(getDriver());
    }
}

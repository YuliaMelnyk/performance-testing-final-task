package tests;

import lombok.extern.log4j.Log4j2;
import org.testng.Assert;
import org.testng.annotations.Test;

@Log4j2
public class LoadingPagesTests extends BaseTests {


    @Test(priority = 1)
    public void openHomePage() {
        getBasePage().waitForPageLoadComplete(100);
        perfNavigationTiming.writeToInflux("HomePage");
        log.info("Home page is loaded");
        Assert.assertTrue(getHomePage().checkClickableComputersButton(), "HomePage isn't loaded");
    }

    @Test(priority = 2)
    public void openComputersPage() {
        getHomePage().computersButtonClick();
        getBasePage().waitForPageLoadComplete(100);
        perfNavigationTiming.writeToInflux("ComputersPage");
        log.info("Computers page is loaded");
        Assert.assertTrue(getComputersPage().checkClickableDesktopsButton(), "ComputersPage isn't loaded");
    }

    @Test(priority = 3)
    public void openDesktopsPage() {
        getHomePage().computersButtonClick();
        getBasePage().waitVisibilityOfElement(5000, getComputersPage().getDesktopsButton());
        getComputersPage().desktopsButtonClick();
        getBasePage().waitElementToBeClickable(getDesktopsPage().getDesktopsTitle());
        perfNavigationTiming.writeToInflux("DesktopsPage");
        log.info("Desktops page is loaded");
        Assert.assertTrue(getDesktopsPage().productsIsEnabled(), "DesktopsPage isn't loaded");
    }

    @Test(priority = 4)
    public void openProductPage() {
        getHomePage().computersButtonClick();
        getBasePage().waitElementToBeClickable(getComputersPage().getDesktopsButton());
        getComputersPage().desktopsButtonClick();
        getBasePage().waitVisibilityOfElement(5000, getDesktopsPage().getDesktopsTitle());
        getDesktopsPage().productsClick();
        getBasePage().waitElementToBeClickable(getProductPage().getAddToCartButton());
        perfNavigationTiming.writeToInflux("ProductPage");
        log.info("Product page is loaded");
        Assert.assertTrue(getProductPage().addToCartButtonIsEnabled(), "ProductPage isn't loaded");
    }
}

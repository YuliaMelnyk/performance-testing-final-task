package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ComputersPage extends BasePage{

    @FindBy(xpath = "//ul[contains(@class,'sublist')]//li[contains(@class,'inactive')]//a[contains(@href,'desktops')]")
    private WebElement desktopsButton;

    //ul[contains(@class,'sublist')]//li[contains(@class,'inactive')]//a[contains(@href,'desktops')]
//div[contains(@class,'sub-category')]//h2//a[contains(@href,'desktops')]
    public WebElement getDesktopsButton() {
        return desktopsButton;
    }

    public ComputersPage(WebDriver driver) {
        super(driver);
    }

    public boolean checkClickableDesktopsButton(){
        return desktopsButton.isEnabled();
    }

    public void desktopsButtonClick(){
        desktopsButton.click();
    }
}


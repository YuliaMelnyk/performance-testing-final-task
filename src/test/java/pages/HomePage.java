package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage{

@FindBy(xpath = "//ul[contains(@class,'notmobile')]//a[contains(@href,'computers')]")
private WebElement computersButton;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public boolean checkClickableComputersButton(){
        return computersButton.isEnabled();
    }

    public void computersButtonClick(){
        computersButton.click();
    }
}



package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Random;

public class DesktopsPage extends BasePage{

    @FindBy(className = "product-title")
    private List<WebElement> products;

    @FindBy(xpath = "//div[contains(@class,'page-title')]//h1")
    private WebElement desktopsTitle;

    public DesktopsPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getDesktopsTitle(){
        return desktopsTitle;
    }

    public boolean productsIsEnabled(){
        for (WebElement product : products) {
           if(!product.isEnabled()){
               return false;
           }
        }
        return true;
    }

    public void productsClick(){
        Random random = new Random();
        int productNumber = random.nextInt(products.size());
        if (productNumber == 0){
            productNumber = 1;
        }
        products.get(productNumber).click();
    }
}

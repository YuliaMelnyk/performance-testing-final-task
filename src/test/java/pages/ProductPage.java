package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductPage extends BasePage{

    @FindBy(xpath = "//input[contains(@id,'add-to-cart')]")
    private WebElement addToCartButton;

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public boolean addToCartButtonIsEnabled(){
        return addToCartButton.isEnabled();
    }

    public WebElement getAddToCartButton(){
        return addToCartButton;
    }
}
